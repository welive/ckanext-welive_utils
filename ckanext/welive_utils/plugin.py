import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import ckan.model as model
import ckan.lib.dictization.model_dictize as model_dictize
import ckan.logic as logic
import ckan.authz as authz
from ckan.logic import action
from ckan.model.package import Package
from ckan.model.group import Group
from ckan.model.resource import Resource
from ckan.model.rating import Rating
from ckan.lib.base import c
from ckanext.welive import utils
from ckan.logic.auth import create as auth_create, update as auth_update
from ckan.logic.auth import delete as auth_delete
from ckanext.welive_authentication import plugin as auth_plugin
from ckan.lib.celery_app import celery
import ckan.lib.search as search
import logging
import ConfigParser
import os
import requests
import json
import redis
import pylons
import uuid

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

PLUGIN_SECTION = 'plugin:logging'
LOGGING_URL = config.get(PLUGIN_SECTION, 'logging_url')
APP_ID = config.get(PLUGIN_SECTION, 'app_id')

WELIVE_SECTION = 'plugin:welive_utils'
WELIVE_API = config.get(WELIVE_SECTION, 'welive_api')

BASIC_USER = config.get(WELIVE_SECTION, 'basic_user')
BASIC_PASSWORD = config.get(WELIVE_SECTION, 'basic_password')

BASIC_USER = config.get(WELIVE_SECTION, 'basic_user')
BASIC_PASSWORD = config.get(WELIVE_SECTION, 'basic_password')

REDIS_SECTION = 'redis'
REDIS_HOST = config.get(REDIS_SECTION, 'redis.host')
REDIS_PORT = config.get(REDIS_SECTION, 'redis.port')
REDIS_DATABASE = config.get(REDIS_SECTION, 'redis.mkt.database')

MAIN_SECTION = 'app:main'
WELIVE_URL = config.get(MAIN_SECTION, 'ckan.site_url')

AUTH_SECTION = 'plugin:authentication'
CLIENT_TOKEN = config.get(AUTH_SECTION, 'client_token')
ADMIN_CC_USER_ID = config.get(AUTH_SECTION, 'admin_cc_user_id')

LANG_DICT = {'es': 'spanish', 'it': 'italian',
             'en': 'english', 'fi': 'finnish', 'sr': 'serbian'}


# PILOT_DICT = {'bilbao': 'Bilbao', 'novi-sad': 'Novisad',
#               'helsinki-uusimaa': 'Uusimaa', 'trento': 'Trento'}
PILOT_DICT = json.loads(config.get(WELIVE_SECTION, 'pilot_dict'))

log = logging.getLogger(__name__)

NotFound = logic.NotFound
NotAuthorized = logic.NotAuthorized

r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DATABASE)


def update_dataset_owner(dataset, new_owner_id):
    query = "SELECT user_object_role_id FROM package_role " \
        "WHERE package_id = '{}';".format(dataset['id'])

    result = model.Session.execute(query)
    for row in result:
        query = "UPDATE user_object_role SET user_id = '{}' " \
            "WHERE id = '{}' AND role = 'admin'; COMMIT;".format(
                new_owner_id, row[0])
        model.Session.execute(query)
    query = "UPDATE package SET creator_user_id = '{}' " \
        "WHERE id = '{}'; COMMIT;".format(new_owner_id, dataset['id'])
    psi = search.PackageSearchIndex()
    dataset['creator_user_id'] = new_owner_id
    psi.index_package(dataset)
    model.Session.execute(query)


def update_mkt(context, data_dict):
    if 'package_id' in data_dict:
        dataset = action.get.package_show(context,
                                          {'id': data_dict['package_id']})
    else:
        dataset = action.get.package_show(context, {'id': data_dict['id']})

    celery.send_task("welive_utils.update_mkt",
                     args=[dataset],
                     task_id=str(uuid.uuid4()))


def create_mkt(context, data_dict):
    if 'package_id' in data_dict:
        dataset = action.get.package_show(context,
                                          {'id': data_dict['package_id']})
    else:
        dataset = action.get.package_show(context, {'id': data_dict['id']})

    if context.get('auth_user_obj', None) is not None:
        cc_user_id = int(context['auth_user_obj'].name)
    else:
        user = action.get.user_show(
            context, {'id': dataset['creator_user_id']})
        cc_user_id = int(user['name'])

    celery.send_task("welive_utils.create_mkt",
                     args=[dataset, cc_user_id],
                     task_id=str(uuid.uuid4()))


def send_log(context, pkg_dict, msg, _type, id_keyword):
    user = context.get('auth_user_obj', None)
    user_name = None
    if user is not None:
        user_name = user.name

    celery.send_task("welive_utils.send_log",
                     args=[pkg_dict, msg, _type, id_keyword, user_name],
                     task_id=str(uuid.uuid4()))


def send_dataset_log(context, pkg_dict, msg, _type):
    send_log(context, pkg_dict, msg, _type, 'datasetid')


def send_resource_log(context, pkg_dict, msg, _type):
    send_log(context, pkg_dict, msg, _type, 'resourceid')


def string_to_list(string_list):
    try:
        return eval(string_list)
    except:
        return []


def get_user_email():
    token = pylons.session['ckanext-welive-token']
    email = auth_plugin.get_user_email(token)
    return email


def get_user_name_for_id(context, user_id):
    context_model = context['model']
    user = context_model.User.get(user_id)
    if user is None:
        return None
    return user.name


def get_email_from_ccuserid(cc_user_id):
    response = requests.get('{}/aac/accountprofile/profiles?userIds={}'.format(
        WELIVE_API, cc_user_id),
                            headers={'Authorization':
                                     'Bearer {}'.format(CLIENT_TOKEN)}).json()
    for profile in response.get('profiles', []):
        if profile is not None:
            if 'accounts' in profile:
                if 'welive' in profile['accounts']:
                    return profile['accounts']['welive'].get('username', None)
                elif 'google' in profile['accounts']:
                    return profile['accounts']['google'].get(
                        'OIDC_CLAIM_email', None)
                elif 'facebook' in profile['accounts']:
                    return profile['accounts']['facebook'].get('email', None)
    return None


@toolkit.side_effect_free
def package_show(context, data_dict):
    package_dict = action.get.package_show(context, data_dict)
    package = Package.get(package_dict['id'])
    package_dict['ratings'] = package.get_average_rating()
    cc_user_id = get_user_name_for_id(context,
                                      package_dict['creator_user_id'])
    email = None
    package_dict['author_email'] = None
    if cc_user_id is not None:
        email = get_email_from_ccuserid(cc_user_id)
        package_dict['author_email'] = email
    if package_dict['type'] == 'dataset':
        send_dataset_log(context, package_dict, 'Dataset metadata accessed',
                         'DatasetMetadataAccessed')
    return package_dict


def package_create(context, data_dict):
    # if type(data_dict) is dict:
    #     log.debug('Creating mapping...')
    #     mapped_resources = []
    #     if 'resources' in data_dict:
    #         for resource in data_dict['resources']:
    #             mapped_resource = generate_mapping(context, resource)
    #             mapped_resources.append(mapped_resource)
    #         data_dict['resources'] = mapped_resources
    log.debug(data_dict)
    if ('language' not in data_dict and
            data_dict.get('type', 'dataset') == 'dataset'):
        found = False
        for extra in data_dict.get('extras', []):
            if extra.get('key', '') == 'language':
                data_dict['language'] = extra.get('value')
                found = True
                data_dict.get('extras').remove(extra)
                break
        if not found:
            raise logic.ValidationError('Missing "language" property')
    package_dict = action.create.package_create(context, data_dict)
    # Failing when harvesting
    # create.follow_dataset(context, package_dict)
    package = None
    if type(package_dict) is not dict:
        package = action.get.package_show(context, {'id': package_dict})
    else:
        package = action.get.package_show(context, {'id': package_dict['id']})
    if package is not None:
        if package['type'] == 'dataset':
            send_dataset_log(context, package, 'Dataset created',
                             'DatasetPublished')
    return package_dict


def package_update(context, data_dict):
    # mapped_resources = []
    # if 'resources' in data_dict:
    #     for resource in data_dict['resources']:
    #         mapped_resource = generate_mapping(context, resource)
    #         mapped_resources.append(mapped_resource)
    #     data_dict['resources'] = mapped_resources

    package_dict = action.update.package_update(context, data_dict)
    if package_dict['type'] == 'dataset':
        update_mkt(context, data_dict)
        send_dataset_log(context, package_dict, 'Dataset updated',
                         'DatasetMetadataUpdated')
    return package_dict


def package_delete(context, data_dict):
    model = context['model']
    package = Package.get(data_dict['id'])
    send_dataset_log(context, model_dictize.package_dictize(package, context),
                     'Dataset removed', 'DatasetRemoved')
    r.delete(package.name)
    action.delete.package_delete(context, data_dict)
    package.purge()
    model.repo.commit_and_remove()
    return None


@toolkit.side_effect_free
def resource_show(context, data_dict):
    resource_dict = action.get.resource_show(context, data_dict)
    # send_resource_log(context, resource_dict, 'Resource metadata accessed',
    #                   'ResourceMetadataAccessed')

    return resource_dict


def resource_create(context, data_dict):
    log.debug('Creating resource for package {}'.format(
        data_dict['package_id']))
    if 'url' not in data_dict:
        url = ""
        data_dict['url'] = url
    resource_dict = action.create.resource_create(context, data_dict)
    try:
        if data_dict.get('mapping', '') == '':
            log.debug('Creating mapping for resource {}'.format(
                resource_dict['id']))
            resource_dict['mapping'] = utils.generate_mapping(context,
                                                              resource_dict)
    except:
        log.error('Error on generating mapping')
    resource_dict = action.update.resource_update(context, resource_dict)
    create_mkt(context, data_dict)
    send_resource_log(context, resource_dict, 'Resource created',
                      'ResourcePublished')

    return resource_dict


def resource_update(context, data_dict):
    log.debug('Updating resource {}'.format(data_dict['id']))
    model = context['model']
    # TODO: return not found
    resource = Resource.get(data_dict['id'])
    extras = resource.extras
    resource_dict = model_dictize.resource_dictize(resource, context)
    for key, value in data_dict.iteritems():
        resource_dict[key] = value

    try:
        if data_dict.get('mapping', '') == '':
            log.debug('Creating mapping for resource {}'.format(
                resource_dict['id']))
            resource_dict['mapping'] = utils.generate_mapping(context,
                                                              resource_dict)
    except:
        log.error('Error on generating mapping')

    resource_dict = action.update.resource_update(context, resource_dict)
    resource = Resource.get(data_dict['id'])
    if len(resource.extras) <= 0:
        resource.extras = extras
    model.repo.commit()
    for key in resource.extras:
        resource_dict[key] = resource.extras[key]

    send_resource_log(context, resource_dict, 'Resource metadata updated',
                      'ResourceMetadataUpdated')

    return resource_dict


def resource_delete(context, data_dict):
    action.delete.resource_delete(context, data_dict)
    send_resource_log(context, data_dict, 'Resource removed',
                      'ResourceRemoved')


def rating_create(context, data_dict):
    return_dict = action.create.rating_create(context, data_dict)
    package_dict = package_show(context, {'id': data_dict['package']})
    send_dataset_log(context, package_dict, 'Dataset updated',
                     'DatasetMetadataUpdated')
    return return_dict


@toolkit.side_effect_free
def user_delete(context, data_dict):
    print context
    user = context.get('auth_user_obj', None)
    if user is None and context.get('user', None) is not None:
        user = context['model'].User.by_name(context['user'])
    if user is None:
        raise NotAuthorized('You must be authenticated to perform this operation')
    else:
        # Get user
        context_model = context['model']
        user_model = context_model.User.get(user.id)
        if data_dict.get('cascade', 'false') == 'true':
            user_info = action.get.user_show(context,
                                             {'id': user.id,
                                              'include_datasets': True})
            for dataset in user_info.get('datasets', []):
                package_delete(context, {'id': dataset['id']})
        else:
            # Check if the user belongs to any organization
            response = requests.post(
                '{}/lum/get-user-company-details/ccuserid/'
                '{}'.format(WELIVE_API, int(user_model.name)),
                auth=(BASIC_USER, BASIC_PASSWORD)).json()
            if not response['error']:
                if not response['isCompanyLeader']:
                    # If user belongs to an organization, assign all her artefacts
                    # to the leader
                    company_id = response['companyId']
                    response = requests.post(
                        '{}/lum/get-leader-ccuserid-by-organizationid/organization-id/'
                        '{}'.format(WELIVE_API, company_id),
                        auth=(BASIC_USER, BASIC_PASSWORD)).json()
                    leader_id = response['ccUserId']
                    # Leader must be null if she doesn't never log into the ODS
                    leader_id = None
                    try:
                        leader = user_by_ccuserid(context, {'cc_user_id': leader_id})
                        leader_id = leader['id']
                    except NotFound:
                        # Leader must be null if she doesn't never log into the ODS
                        leader = auth_plugin.create_user(leader_id)
                        leader_id = leader.id
                    # Assign user's datasets to leader
                    leader = action.get.user_show(context, {'id': leader_id})
                    user_info = action.get.user_show(context,
                                                     {'id': user.id,
                                                      'include_datasets': True})
                    ckan_company_id = None
                    for org_id in action.get.organization_list(context, {}):
                        organization = action.get.organization_show(context, {'id': org_id})
                        for extra in organization['extras']:
                            if extra['key'] == 'orgID' and int(extra['value']) == int(company_id):
                                ckan_company_id = organization['id']
                                break
                    for dataset in user_info.get('datasets', []):
                        if dataset['owner_org'] == ckan_company_id:
                            dataset = action.get.package_show(context, {'id': dataset['id']})
                            update_dataset_owner(dataset, leader['id'])
        # Delete user
        requests.delete('{}/ods/dataset/user/{}'.format(
            WELIVE_API, int(user_model.name)),
            auth=(BASIC_USER, BASIC_PASSWORD))
        user_model.purge()
        context_model.repo.commit()


@toolkit.side_effect_free
def rating_show(context, data_dict):
    if context.get('auth_user_obj', None) is None:
        raise logic.NotAuthorized("A valid token has to be provided through "
                                  "the Authorization header")

    user = context['auth_user_obj']
    try:
        dataset = action.get.package_show(context, data_dict)
    except logic.NotFound:
        raise logic.NotFound
    session = context['session']
    rating = session.query(Rating).filter(Rating.package_id == dataset['id'],
                                          Rating.user_id == user.id).first()
    package = Package.get(data_dict['id'])
    avg_rating = package.get_average_rating()
    rating_count = len(package.ratings)
    rating_dict = {'avg_rating': avg_rating, 'rating_count': rating_count}

    if rating is not None:
        rating_dict['user_rating'] = {'ccUserId': int(user.name),
                                      'rating': rating.rating}
        return rating_dict
    else:
        rating_dict['user_rating'] = {'ccUserId': int(user.name), 'rating': None}
        return rating_dict


@toolkit.side_effect_free
def get_organization_id(context, data_dict):
    selected_organization = None
    for organization in action.get.organization_list(context, {'all_fields': True, 'include_extras': True, 'include_users': True}):
        for extra in organization['extras']:
            if extra['key'] == 'orgID' and int(extra['value']) == int(data_dict['id']):
                selected_organization = organization
                break
        if selected_organization is not None:
            break
    if selected_organization is not None:
        return selected_organization
    else:
        raise NotFound


def send_dataset_log_helper(pkg_dict, msg, _type):
    send_dataset_log({'auth_user_obj': c.userobj}, pkg_dict, msg, _type)


def send_resource_log_helper(resource_dict, msg, _type):
    send_resource_log({'auth_user_obj': c.userobj}, resource_dict, msg, _type)


@toolkit.side_effect_free
def user_by_email(context, data_dict):
    email = data_dict.get("email", None)
    if email:
        user_obj_list = model.User.by_email(email)
        user_obj = None
        if (len(user_obj_list) > 0):
            user_obj = user_obj_list[0]
        else:
            raise NotFound
        user_dict = model_dictize.user_dictize(user_obj, context)
        return user_dict
    else:
        raise NotFound


@toolkit.side_effect_free
def user_by_ccuserid(context, data_dict):
    cc_user_id = data_dict.get("cc_user_id", None)
    if cc_user_id:
        for user in model.User.all():
            try:
                if user.name in ['default', 'visitor', 'logged_in']:
                    raise NotFound
                elif int(user.name) == int(cc_user_id):
                    return model_dictize.user_dictize(user, context)
            except Exception as e:  # pragma: no cover
                log.error(e)
    raise NotFound


@toolkit.side_effect_free
def package_create_rest(context, data_dict):
    language = None
    if ('language' not in data_dict and
            data_dict.get('type', 'dataset') == 'dataset'):
        found = False
        if 'language' in data_dict.get('extras', []):
            language = data_dict.get('extras').get('language')
            found = True
            # del data_dict.get('extras')['language']
        if not found:
            raise logic.ValidationError('Missing "language" property')

    package_dict = action.create.package_create_rest(context, data_dict)
    package_dict = package_show(context, {'id': package_dict['id']})

    package_dict['language'] = language

    package_dict = package_update(context, package_dict)
    create_mkt(context, data_dict)
    return package_dict


def organization_delete(context, data_dict):
    model = context['model']
    organization = Group.get(data_dict['id'])
    result = action.delete.organization_delete(context, data_dict)
    organization.purge()
    model.repo.commit_and_remove()
    return result


def organization_list_for_user(context, data_dict):
    org_list = []
    if not context.get('user', '') == '':
        if int(context['user']) == int(ADMIN_CC_USER_ID):
            for organization_id in action.get.organization_list(context, {}):
                organization = action.get.organization_show(
                    context, {'id': organization_id})
                org_list.append(organization)
            return org_list
        response = requests.post("%s/lum/get-user-roles/%s" % (WELIVE_API, int(context['user'])), auth=(BASIC_USER, BASIC_PASSWORD)).json()
        if 'role' in response:
            if response['role'] == 'Authority':
                response = requests.post("%s/lum/get-user-data/cc-user-id/%s" % (WELIVE_API, int(context['user'])), auth=(BASIC_USER, BASIC_PASSWORD)).json()
                referred_pilot = response.get('user', {}).get('referredPilot', None)
                for k, v in PILOT_DICT.items():
                    if v == referred_pilot:
                        pilot = action.get.organization_show(context, {'id': k})
                        org_list.append(pilot)
            elif response['isDeveloper']:
                response = requests.get('{}/cdv/getuserprofile'.format(WELIVE_API), headers={'Authorization': pylons.session['ckanext-welive-token']}).json()
                referred_pilot = response.get('referredPilot', None)
                if referred_pilot is not None:
                    for k, v in PILOT_DICT.items():
                        if v == referred_pilot:
                            organization = action.get.organization_show(context, {'id': k})
                            org_list.append(organization)

        response = requests.post("%s/lum/get-user-company-details/ccuserid/%s" % (WELIVE_API, int(context['user'])), auth=(BASIC_USER, BASIC_PASSWORD)).json()
        companyId = response.get('companyId', None)
        if companyId is not None:
            organization = None
            for organization in action.get.organization_list(context, {'all_fields': True, 'include_extras': True, 'include_users': True}):
                for extra in organization.get('extras', []):
                    if extra['key'] == 'orgID' and int(extra['value']) == int(companyId):
                        org_list.append(organization)
    return org_list


def can_update(context, data_dict):
    if data_dict is None:
        package = context['package']
        data_dict = model_dictize.package_dictize(package, context)
    if data_dict is not None:
        if 'id' in data_dict or 'package_id' in data_dict:
            log.debug(data_dict)
            user = context['auth_user_obj']
            package = data_dict
            if package is None:
                package = context['package']
                creator_user_id = package.creator_user_id
            else:
                model = context['model']
                if 'id' in data_dict:
                    package = model.Package.get(data_dict['id'])
                else:
                    package = model.Package.get(data_dict['package_id'])
                if package is not None:
                    creator_user_id = package.creator_user_id
                else:
                    resource = model.Resource.get(data_dict['id'])
                    package = model.Package.get(resource.package_id)
                    creator_user_id = package.creator_user_id

            if None not in [user, package]:
                log.debug('Getting user roles for user: {}'.format(
                    int(user.name)))
                response = requests.post(
                    "%s/lum/get-user-roles/%s" % (WELIVE_API, int(user.name)),
                    auth=(BASIC_USER, BASIC_PASSWORD))

                allowed = False
                if response.status_code == 200:
                    try:
                        json_response = response.json()
                    except Exception as e:
                        log.error(e)
                        return False
                    if not json_response["error"]:
                        if json_response["role"] == "Authority":
                            allowed = True
                        if json_response["isDeveloper"]:
                            allowed = True
                if allowed:
                    if creator_user_id == user.id:
                        return True
    return False


@toolkit.side_effect_free
def get_qm_data(context, data_dict):
    dataset = data_dict['dataset']
    resource = data_dict['resource']
    query = data_dict['query']
    origin = data_dict.get('origin', 'ANY')

    response = requests.post('{}/ods/{}/resource/{}/query?origin={}'.format(
        WELIVE_API, dataset, resource, origin
    ), data=query)

    return response.json()


def auth_package_create(context, data_dict):
    log.debug("Auth package create")
    user = context['auth_user_obj']
    if user is not None:
        response = requests.post("%s/lum/get-user-roles/%s" % (WELIVE_API,
                                                               user.name),
                                 auth=(BASIC_USER, BASIC_PASSWORD))

        if response.status_code == 200:
            json_response = response.json()
            if not json_response["error"]:
                if json_response["role"] == "Authority":
                    return {'success': True}
                elif json_response["isDeveloper"]:
                    return {'success': True}
                else:
                    return {'success': False}

    return auth_create.package_create(context, data_dict)


def auth_resource_create(context, data_dict):
    log.debug("Auth resource create")
    if can_update(context, data_dict):
        return {'success': True}

    return auth_create.resource_create(context, data_dict)


def auth_package_update(context, data_dict):
    log.debug("Auth package update")
    if can_update(context, data_dict):
        log.debug('The user can update the package')
        return {'success': True}

    return auth_update.package_update(context, data_dict)


def auth_resource_update(context, data_dict):
    log.debug("Auth resource update")
    if can_update(context, data_dict):
        return {'success': True}

    return auth_update.resource_update(context, data_dict)


@toolkit.auth_allow_anonymous_access
def auth_package_show(context, data_dict):
    log.debug('Auth package show for dataset or resource {}'.format(
        data_dict['id']))
    package = None
    context['ignore_auth'] = True
    package = action.get.package_show(context, {'id': data_dict['id']})
    if package is None:
        resource = action.get.resource_show(context, {'id': data_dict['id']})
        package = action.get.package_show(context, {'id': resource['package_id']})
    if package is not None:
        if not package['private']:
            return {'success': True}
        elif context['auth_user_obj'] is None:
            return {'success': False}
        else:
            user = context['auth_user_obj']
            response = requests.post("%s/lum/get-user-roles/%s" % (WELIVE_API, int(user.name)), auth=(BASIC_USER, BASIC_PASSWORD)).json()
            role = None
            if 'role' in response:
                role = response['role']
            if role is not None:
                organization = action.get.organization_show(context, {'id': package['owner_org']})
                if organization['name'] in PILOT_DICT:
                    if package['creator_user_id'] == user.id:
                        return {'success': True}
                    elif role == "Authority":
                        return {'success': True}
                else:
                    org_id = None
                    for extra in organization['extras']:
                        if extra['key'] == 'orgID':
                            org_id = extra['value']
                            break
                    if org_id is not None:
                        response = requests.post("%s/lum/get-user-company-details/ccuserid/%s" % (WELIVE_API, int(user.name)), auth=(BASIC_USER, BASIC_PASSWORD)).json()
                        if 'companyId' in response:
                            if int(response['companyId']) == int(org_id):
                                return {'success': True}

    return {'success': False}


def auth_package_delete(context, data_dict):
    log.debug("Auth package delete")
    if can_update(context, data_dict):
        return {'success': True}

    return auth_delete.package_delete(context, data_dict)


def auth_resource_delete(context, data_dict):
    log.debug("Auth resource delete")
    if can_update(context, data_dict):
        return {'success': True}

    return auth_delete.resource_delete(context, data_dict)


def auth_user_update(context, data_dict):
    return {'success': False}


def auth_user_list(context, data_dict):
    return {'success': True}


def auth_user_show(context, data_dict):
    return {'success': True}


def auth_organization_create(context, data_dict):
    user = context['user']
    user = authz.get_user_id_for_username(user, allow_none=True)

    if user:
        return {'success': True}


class Welive_UtilsPlugin(plugins.SingletonPlugin, toolkit.DefaultDatasetForm):
    plugins.implements(plugins.IAuthFunctions)
    plugins.implements(plugins.IConfigurer, inherit=False)
    plugins.implements(plugins.IActions)
    plugins.implements(plugins.IDatasetForm, inherit=False)
    plugins.implements(plugins.ITemplateHelpers)

    # IAuthFunctions
    def get_auth_functions(self):
        return {'package_show': auth_package_show,
                'package_create': auth_package_create,
                'resource_create': auth_resource_create,
                'package_update': auth_package_update,
                'resource_update': auth_resource_update,
                'package_delete': auth_package_delete,
                'resource_delete': auth_resource_delete,
                'user_update': auth_user_update,
                'user_list': auth_user_list,
                'user_show': auth_user_show,
                'organization_create': auth_organization_create}

    # ITemplateHelpers
    def get_helpers(self):
        return {'send_dataset_log_helper': send_dataset_log_helper,
                'send_resource_log_helper': send_resource_log_helper,
                'string_to_list': string_to_list,
                'get_user_email': get_user_email}

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'welive_utils')

    # IActions

    def get_actions(self):
        return {'package_show': package_show,
                'package_create': package_create,
                'package_update': package_update,
                'package_delete': package_delete,
                'resource_show': resource_show,
                'resource_create': resource_create,
                'resource_update': resource_update,
                'resource_delete': resource_delete,
                'rating_create': rating_create,
                'rating_show': rating_show,
                'user_by_email': user_by_email,
                'package_create_rest': package_create_rest,
                'organization_list_for_user': organization_list_for_user,
                'user_delete': user_delete,
                'get_organization_id': get_organization_id,
                'organization_delete': organization_delete,
                'user_by_ccuserid': user_by_ccuserid,
                'get_qm_data': get_qm_data}

    # IDatasetForm

    def is_fallback(self):
        # Return True to register this plugin as the default handler for
        # package types not handled by any other IDatasetForm plugin.
        return False

    def package_types(self):
        # This plugin doesn't handle any special package types, it just
        # registers itself as the default (above).
        return ['dataset']

    def create_package_schema(self):
        schema = super(Welive_UtilsPlugin, self).create_package_schema()
        schema.update({
            'language': [toolkit.get_converter('convert_to_extras'),
                         toolkit.get_validator('ignore_missing')],
            'owner_org': [unicode]
        })
        return schema

    def update_package_schema(self):
        schema = super(Welive_UtilsPlugin, self).update_package_schema()
        # our custom field
        schema.update({
            'language': [toolkit.get_converter('convert_to_extras'),
                         toolkit.get_validator('ignore_missing')],
            'owner_org': [toolkit.get_validator('ignore_missing')]
        })
        return schema

    def show_package_schema(self):
        schema = super(Welive_UtilsPlugin, self).show_package_schema()
        schema.update({
            'language': [toolkit.get_converter('convert_from_extras'),
                         toolkit.get_validator('ignore_missing')],
            'owner_org': []
        })
        return schema
