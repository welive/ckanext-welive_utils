import ConfigParser
import os
import json
import requests
import traceback
import logging
import redis
import time
from ckan.lib.celery_app import celery
from dateutil import parser

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

WELIVE_SECTION = 'plugin:welive_utils'
PILOT_DICT = json.loads(config.get(WELIVE_SECTION, 'pilot_dict'))
WELIVE_API = config.get(WELIVE_SECTION, 'welive_api')
BASIC_USER = config.get(WELIVE_SECTION, 'basic_user')
BASIC_PASSWORD = config.get(WELIVE_SECTION, 'basic_password')

MAIN_SECTION = 'app:main'
WELIVE_URL = config.get(MAIN_SECTION, 'ckan.site_url')

REDIS_SECTION = 'redis'
REDIS_HOST = config.get(REDIS_SECTION, 'redis.host')
REDIS_PORT = config.get(REDIS_SECTION, 'redis.port')
REDIS_DATABASE = config.get(REDIS_SECTION, 'redis.mkt.database')

PLUGIN_SECTION = 'plugin:logging'
LOGGING_URL = config.get(PLUGIN_SECTION, 'logging_url')
APP_ID = config.get(PLUGIN_SECTION, 'app_id')


LANG_DICT = {'es': 'spanish', 'it': 'italian',
             'en': 'english', 'fi': 'finnish', 'sr': 'serbian'}

r = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DATABASE)

log = logging.getLogger(__name__)


@celery.task(name="welive_utils.update_mkt")
def update_mkt(dataset):
    try:
        if dataset.get('type', 'harvest') != 'harvest':
            mkp_json = {}
            mkp_json['datasetid'] = dataset['name']
            mapped = False
            for resource in dataset.get('resources', []):
                if resource.get('mapping', '{}') != '{}':
                    mapped = True
                    break
            mkp_json['hasmapping'] = mapped
            # At this moment there is no agreement about data to be sent to
            # Marketplace
            mkp_json['model'] = {'title': dataset.get('title',
                                                      'Untitled dataset')}
            if dataset.get('notes', 'No description provided') == '':
                mkp_json['model']['description'] = 'No description provided'
            else:
                mkp_json['model']['description'] = dataset['notes']
            mkp_json['model']['pilot'] = PILOT_DICT[dataset['organization']['name']]
            pdate = parser.parse(dataset['metadata_created'])
            mkp_json['model']['created'] = '{}/{}/{}'.format(
                pdate.year, pdate.month, pdate.day)
            mkp_json['model']['page'] = '{}/ods/dataset/{}'.format(
                WELIVE_API, dataset['name'])
            mkp_json['model']['tags'] = []
            for tag in dataset['tags']:
                mkp_json['model']['tags'].append(tag['name'])
            if dataset.get('notes', 'No abstract provided') == '':
                mkp_json['model']['_abstract'] = 'No abstract provided'
            else:
                mkp_json['model']['_abstract'] = dataset['notes']
            mkp_json['model']['language'] = LANG_DICT[dataset.get(
                'language', '')].capitalize()

            business_role = {
                'title': dataset['organization']['title'],
                'page': '{}/organization/{}'.format(
                    WELIVE_URL, dataset['organization']['name']),
                'businessRole': 'AUTHOR',
                'mbox': dataset.get('author_email', None)
                             }
            mkp_json['model']['hasBusinessRole'] = []
            mkp_json['model']['hasBusinessRole'].append(business_role)

            legal_condition = {'title': dataset['license_id'],
                               'description': dataset['license_title'],
                               'url': dataset.get('license_url', None)}
            mkp_json['model']['hasLegalCondition'] = []
            mkp_json['model']['hasLegalCondition'].append(legal_condition)
            mkp_json['isPrivate'] = dataset['private']

            log.debug(json.dumps(mkp_json))
            response = requests.post('{}/mkp/update-dataset'.format(
                WELIVE_API), data=json.dumps(mkp_json),
                auth=(BASIC_USER, BASIC_PASSWORD),
                headers={'Content-type': 'application/json'})
            log.debug('Update notification about dataset {} sent '
                      'to Marketplace'.format(dataset['id']))
            log.debug('Update response: {}'.format(response.text))
    except Exception:
        traceback.print_exc()


@celery.task(name="welive_utils.create_mkt")
def create_mkt(dataset, cc_user_id):
    try:
        if dataset.get('type', 'harvest') != 'harvest':
            mkt_id = None
            if not r.exists(dataset['name']):
                pilot = PILOT_DICT[dataset['organization']['name']]
                response = requests.get(
                    '{}/mkp/get-all-artefacts/pilot-id/{}/artefact-types/'
                    'Dataset'.format(WELIVE_API, pilot),
                    auth=(BASIC_USER, BASIC_PASSWORD))
                mkt_datasets = response.json()
                for item in mkt_datasets['artefacts']:
                    if item['eId'] == dataset['name']:
                        mkt_id = item['artefactId']
                        r.set(dataset['name'], mkt_id)
                        log.debug('redis_set')
                        log.debug(mkt_id)
                        break
                if mkt_id is None:
                    mkp_json = {}
                    mkp_json['title'] = dataset.get('title',
                                                    'Untitled dataset')
                    if dataset.get('notes', 'No description provided') == '':
                        mkp_json['description'] = 'No description provided'
                    else:
                        mkp_json['description'] = dataset['notes']
                    mkp_json['resourceRdf'] = '{}/ods/dataset/{}/usdl'.format(
                            WELIVE_API, dataset['name'])
                    mkp_json['webpage'] = '{}/ods/dataset/{}'.format(
                        WELIVE_API, dataset['name'])
                    mkp_json['providerName'] = dataset.get(
                        'organization', {}).get('title',
                                                'No organization provided')
                    mkp_json['datasetId'] = dataset['name']
                    mkp_json['language'] = LANG_DICT[dataset.get(
                        'language', '')].capitalize()

                    mkp_json['ccUserId'] = cc_user_id

                    mapped = False
                    for resource in dataset.get('resources', []):
                        if resource.get('mapping', '{}') != '{}':
                            mapped = True
                            break
                    mkp_json['hasmapping'] = mapped

                    log.debug(json.dumps(mkp_json))

                    response = requests.post('{}/mkp/create-dataset'.format(
                        WELIVE_API), data=json.dumps(mkp_json),
                        auth=(BASIC_USER, BASIC_PASSWORD),
                        headers={'Content-type': 'application/json'})
                    response_json = response.json()
                    if 'artefactid' in response_json:
                        mkt_id = response_json['artefactid']
                        r.set(dataset['name'], mkt_id)
                    else:
                        log.error('Error when notifying dataset {} to the '
                                  'marketplace: sent JSON: {}\n Received '
                                  'response: {}'.format(dataset['name'],
                                                        mkp_json,
                                                        response.content))
    except Exception:
        traceback.print_exc()


@celery.task(name="welive_utils.send_log")
def send_log(pkg_dict, msg, _type, id_keyword, user_name):
    current_time = time.time()
    log.debug('%s at %s' % (msg, current_time))
    if id_keyword == 'datasetid':
        custom_attr = {id_keyword: pkg_dict["name"]}
        if 'qa' in pkg_dict:
            qa = pkg_dict['qa']
            custom_attr['datasetopenness'] = qa['openness_score']
            if 'openness_score_reason' in pkg_dict['qa']:
                openess_reason = qa['openness_score_reason']
                custom_attr['datasetopennesreason'] = openess_reason
    else:
        custom_attr = {id_keyword: pkg_dict["id"]}
    if user_name is not None:
        try:
            if _type in ['DatasetPublished', 'DatasetMetadataUpdated',
                         'DatasetRemoved', 'ResourcePublished',
                         'ResourceUpdated', 'ResourceRemoved',
                         'ResourceMetadataUpdated']:
                custom_attr['authorid'] = int(user_name)
            else:
                custom_attr['userid'] = int(user_name)
        except ValueError:
            log.debug('User {} has no integer user name!'.format(user_name))
    if 'title' in pkg_dict:
        if pkg_dict['title'] is not None:
            custom_attr['datasetname'] = pkg_dict['title']
    if 'ratings' in pkg_dict:
        if pkg_dict['ratings'] is not None:
            custom_attr['rating'] = pkg_dict['ratings']
    if 'organization' in pkg_dict:
        custom_attr['pilot'] = PILOT_DICT.get(pkg_dict['organization']['name'],
                                              None)
    if 'type' in pkg_dict:
        if pkg_dict['type'] == 'dataset':
            if not pkg_dict['private']:
                custom_attr['datasettype'] = 'Open Data'
            else:
                custom_attr['datasettype'] = 'Private Data'
    if 'extras' in pkg_dict:
        for extra in pkg_dict['extras']:
            if 'key' in extra:
                if extra['key'] == 'source':
                    custom_attr['datasettype'] = extra['value']
    if 'format' in pkg_dict:
        custom_attr['resourcetype'] = pkg_dict['format']

    data = {'msg': msg,
            'appId': APP_ID,
            'type': _type,
            'timestamp': current_time,
            'custom_attr': custom_attr
            }
    try:
        response = requests.post("%s/%s" % (LOGGING_URL, APP_ID),
                                 data=json.dumps(data),
                                 headers={'Content-type': 'application/json'},
                                 auth=(BASIC_USER, BASIC_PASSWORD))
        if response.status_code >= 400:
            log.debug(response.content)
    except:
        log.debug("Can't connect to logging service")
