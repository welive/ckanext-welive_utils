"""Tests for plugin.py."""
from nose.tools import assert_raises
from ckan import logic
from ckanext.welive_utils import plugin
from requests.exceptions import Timeout
import ckan.tests.legacy as tests
import pylons.config as pylons_config
import unittest
import ckan.tests.helpers as helpers
import ckan.tests.factories as factories
import ckan.model as model
import ckan
import logging
import mock
import json
import ConfigParser
import os
import webtest
import time

log = logging.getLogger(__name__)

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

WELIVE_SECTION = 'plugin:welive_utils'
WELIVE_API = config.get(WELIVE_SECTION, 'welive_api')

BASIC_USER = config.get(WELIVE_SECTION, 'basic_user')
BASIC_PASSWORD = config.get(WELIVE_SECTION, 'basic_password')

PACKAGE_DATA = {'name': 'test-package', 'title': "Test Package",
                'language': 'es'}

RESOURCE_DATA = {'name': 'Test Resource', 'format': 'CSV'}

LOGGING_SECTION = 'plugin:logging'
LOGGING_URL = config.get(LOGGING_SECTION, 'logging_url')
APP_ID = config.get(LOGGING_SECTION, 'app_id')

AUTH_SECTION = 'plugin:authentication'
CLIENT_TOKEN = config.get(AUTH_SECTION, 'client_token')

MAPPING = {
   "mapping": "csv",
   "uri": "someuri",
   "key": "someattr",
   "delimiter": ";",
   "refresh": 1000
}

MAPPING_UPDATE = {
   "mapping": "csv",
   "uri": "anotheruri",
   "key": "anotherattr",
   "delimiter": ";",
   "refresh": 1000
}

MKT_POST_RESPONSE = json.dumps({
  "error": False,
  "message": "string",
  "artefactid": 1567
})

MKT_POST_ERROR_RESPONSE = json.dumps({
  "error": True,
  "message": "string",
})

MKT_GET_RESPONSE = json.dumps({
  "artefacts": [
    {
      "artefactId": 1567,
      "name": "string",
      "description": "string",
      "interfaceOperation": "string",
      "type": "string",
      "typeId": 0,
      "rating": 0,
      "url": "string",
      "tags": [
        "string"
      ],
      "comments": [
        {
          "text": "string",
          "author": "string",
          "creation_date": "2016-05-02T07:04:49.216Z"
        }
      ],
      "eId": 'existing-packageID'
    }
  ]
})

LUM_USER_ROLES = json.dumps({
  "isDeveloper": True,
  "error": False,
  "role": "Citizen"
})

LUM_USER_DATA = json.dumps({
  "error": False,
  "user": {
    "lastName": "Bar",
    "ccUserID": "5",
    "referredPilot": "Bilbao",
    "avatar": "avatar_url",
    "isMale": True,
    "country": "",
    "city": "",
    "employement": "",
    "id": 54125,
    "languages": [
      "Spanish",
      "English"
    ],
    "isDeveloper": True,
    "liferayScreenName": "foo",
    "email": "foo@bar.com",
    "address": "",
    "zipCode": "",
    "role": "Citizen",
    "birthDate": {
      "month": 0,
      "year": 1970,
      "day": 1
    },
    "firstName": "Foo"
  }
})

COMPANY_DETAILS = json.dumps({
  "message": "string",
  "error": False,
  "isCompanyLeader": False,
  "companyId": 156,
  "companyName": "Fake Company"
})

ORGANIZATION_LEADER = json.dumps({
  "message": "string",
  "error": False,
  "ccUserId": 3
})

AUTHORITY_ROLE = json.dumps({
  "isDeveloper": True,
  "error": False,
  "role": "Authority"
})

DEVELOPER_ROLE = json.dumps({
  "isDeveloper": True,
  "error": False,
  "role": "Developer"
})

CITIZEN_ROLE = json.dumps({
  "isDeveloper": False,
  "error": False,
  "role": "Citizen"
})

WELIVE_ACCOUNT = json.dumps({
    'profiles': [
        {
            'accounts':
                {
                    'welive': {'username': 'fake@mail.com'}
                }
        }
    ]
})

GMAIL_ACCOUNT = json.dumps({
    'profiles': [
        {
            'accounts':
                {
                    'google': {'OIDC_CLAIM_email': 'fake@mail.com'}
                }
        }
    ]
})

FACEBOOK_ACCOUNT = json.dumps({
    'profiles': [
        {
            'accounts':
                {
                    'facebook': {'email': 'fake@mail.com'}
                }
        }
    ]
})


class MockResponse(object):
    def __init__(self, content, status_code=200):
        self.content = content
        self.status_code = status_code

    def json(self):
        return json.loads(self.content)


class MockedUser(object):
    def __init__(self, name='05', _id='fake-id'):
        self.name = name
        self.id = _id


class MockedRating(object):
    def __init__(self, rating_value):
        self.rating = rating_value


class MockedQuery(object):
    def __init__(self, rating_value):
        self.rating = MockedRating(rating_value)

    def filter(self, arg1, arg2):
        return self

    def first(self):
        return self.rating


class MockedSession(object):
    def __init__(self, rating=4.0):
        self.query_item = MockedQuery(rating)

    def query(self, rating_object):
        return self.query_item


class MockedPackage(object):
    def __init__(self):
        self.avg_rating = 3.0
        self.ratings = [2.0, 4.0]

    def get_average_rating(self):
        return self.avg_rating


def mock_user_dictize(*args):
    return args[0]


def create_dataset(package_data=None, context={},
                   organization_title='Test Organization',
                   organization_name='test-organization',
                   organization_extras=[]):
    org = factories.Organization(title=organization_title,
                                 name=organization_name,
                                 extras=organization_extras)
    if package_data is not None:
        package_data['owner_org'] = org['id']
        params = package_data
    else:
        params = {'name': PACKAGE_DATA['name'], 'title': PACKAGE_DATA['title'],
                  'language': PACKAGE_DATA['language'], 'owner_org': org['id'],
                  'notes': ''}
    dataset = helpers.call_action('package_create', context, **params)
    return dataset


def create_resource(dataset=None, context={}):
    if dataset is None:
        dataset = create_dataset()

    params = {'package_id': dataset['id'], 'name': RESOURCE_DATA['name'],
              'format': RESOURCE_DATA['format'], 'url': 'http://foo/bar'}
    helpers.call_action('resource_create', context, **params)

    params = {'id': dataset['id']}
    dataset = helpers.call_action('package_show', context, **params)

    return dataset['resources'][0]


class TestCreateDataset(unittest.TestCase):
    def setUp(self):
        model.repo.init_db()
        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    def test_create_dataset(self, mock_send_dataset_log,
                            mock_get_user_name_for_id,
                            mock_get_email_for_ccuserid):
        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'

        dataset = create_dataset()
        context = {}
        params = {'id': dataset['id']}
        dataset = helpers.call_action('package_show', context, **params)

        self.assertEqual(dataset['name'], PACKAGE_DATA['name'])
        self.assertEqual(dataset['title'], PACKAGE_DATA['title'])
        self.assertEqual(dataset['language'], PACKAGE_DATA['language'])

        # TODO: Check if passed dataset is correct
        calls = []
        calls.append(mock.call(mock.ANY, mock.ANY, 'Dataset created',
                               'DatasetPublished'))
        calls.append(mock.call(mock.ANY, mock.ANY, 'Dataset metadata accessed',
                               'DatasetMetadataAccessed'))
        mock_send_dataset_log.assert_has_calls(calls, any_order=True)

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    def test_create_dataset_lang_in_extras(self, mock_send_dataset_log,
                                           mock_get_user_name_for_id,
                                           mock_get_email_for_ccuserid):
        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'

        dataset_data = dict(PACKAGE_DATA)
        del dataset_data['language']
        dataset_data['extras'] = [{'key': 'language',
                                   'value': PACKAGE_DATA['language']}]
        dataset = create_dataset(package_data=dataset_data)
        context = {}
        params = {'id': dataset['id']}
        dataset = helpers.call_action('package_show', context, **params)

        self.assertEqual(dataset['name'], PACKAGE_DATA['name'])
        self.assertEqual(dataset['title'], PACKAGE_DATA['title'])
        self.assertEqual(dataset['language'], PACKAGE_DATA['language'])

        # TODO: Check if passed dataset is correct
        calls = []
        calls.append(mock.call(mock.ANY, mock.ANY, 'Dataset created',
                               'DatasetPublished'))
        calls.append(mock.call(mock.ANY, mock.ANY, 'Dataset metadata accessed',
                               'DatasetMetadataAccessed'))
        mock_send_dataset_log.assert_has_calls(calls, any_order=True)

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    def test_create_dataset_lang_exception(self, mock_send_dataset_log,
                                           mock_get_user_name_for_id,
                                           mock_get_email_for_ccuserid):
        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'

        dataset_data = dict(PACKAGE_DATA)
        del dataset_data['language']
        with self.assertRaises(logic.ValidationError):
            dataset = create_dataset(package_data=dataset_data)
            context = {}
            params = {'id': dataset['id']}
            dataset = helpers.call_action('package_show', context, **params)

            mock_send_dataset_log.assert_not_called()

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckan.logic.action.create.package_create_rest')
    @mock.patch('ckanext.welive_utils.plugin.package_show')
    @mock.patch('ckanext.welive_utils.plugin.package_update')
    @mock.patch('ckanext.welive_utils.plugin.create_mkt')
    def test_create_package_rest(self, mock_create_mkt, mock_package_update,
                                 mock_package_show, mock_package_create_rest,
                                 mock_get_user_name_for_id,
                                 mock_get_email_for_ccuserid):
        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'
        dataset = create_dataset()
        mock_package_create_rest.return_value = dict(dataset)
        mock_package_show.return_value = dict(dataset)
        mock_package_update.return_value = dict(dataset)

        result_dataset = plugin.package_create_rest({}, PACKAGE_DATA)

        mock_package_create_rest.assert_called_with({}, PACKAGE_DATA)
        mock_package_show.assert_called_with({}, {'id': dataset['id']})
        mock_package_update.assert_called_with({}, dataset)
        mock_create_mkt.assert_called_with({}, PACKAGE_DATA)

        del dataset['id']
        del result_dataset['id']

        self.assertDictEqual(dataset, result_dataset)

    @mock.patch('requests.post')
    def test_auth_create_dataset_authority(self, mock_post):
        mock_post.return_value = MockResponse(AUTHORITY_ROLE)

        user = factories.User(email='fake@mail.com', name='05')
        context = {'model': model, 'user': user['name']}
        params = {}

        result = helpers.call_auth('package_create', context, **params)

        self.assertTrue(result)

    @mock.patch('requests.post')
    def test_auth_create_dataset_developer(self, mock_post):
        mock_post.return_value = MockResponse(DEVELOPER_ROLE)

        user = factories.User(email='fake@mail.com', name='05')
        context = {'model': model, 'user': user['name']}
        params = {}

        result = helpers.call_auth('package_create', context, **params)

        self.assertTrue(result)

    @mock.patch('requests.post')
    def test_auth_create_dataset_citizen(self, mock_post):
        mock_post.return_value = MockResponse(CITIZEN_ROLE)

        user = factories.User(email='fake@mail.com', name='05')
        context = {'model': model, 'user': user['name']}
        params = {}
        with self.assertRaises(logic.NotAuthorized):
            helpers.call_auth('package_create', context, **params)


class TestUpdateDataset(unittest.TestCase):
    class MockedParse():
        year = 2017
        month = 5
        day = 17

    def setUp(self):
        model.repo.init_db()
        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckanext.welive_utils.plugin.update_mkt')
    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    def test_update_dataset(self, mock_send_dataset_log, mock_update_mkt,
                            mock_get_user_name_for_id,
                            mock_get_email_for_ccuserid):
        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'

        dataset = create_dataset()
        context = {}
        params = {'id': dataset['id']}
        dataset = helpers.call_action('package_show', context, **params)

        self.assertEqual(dataset['name'], PACKAGE_DATA['name'])
        self.assertEqual(dataset['title'], PACKAGE_DATA['title'])
        self.assertEqual(dataset['language'], PACKAGE_DATA['language'])

        update_params = {'id': dataset['id'], 'title': 'Updated Test Package',
                         'language': 'it'}
        helpers.call_action('package_update', context, **update_params)

        params = {'id': dataset['id']}
        dataset = helpers.call_action('package_show', context, **params)

        self.assertEqual(dataset['title'], 'Updated Test Package')
        self.assertEqual(dataset['language'], 'it')

        calls = []
        calls.append(mock.call(mock.ANY, mock.ANY, 'Dataset created',
                               'DatasetPublished'))
        calls.append(mock.call(mock.ANY, mock.ANY, 'Dataset metadata accessed',
                               'DatasetMetadataAccessed'))
        calls.append(mock.call(mock.ANY, mock.ANY, 'Dataset updated',
                               'DatasetMetadataUpdated'))
        mock_send_dataset_log.assert_has_calls(calls, any_order=True)
        update_params['type'] = 'dataset'
        mock_update_mkt.assert_called_with(mock.ANY, update_params)

    @mock.patch('dateutil.parser.parse')
    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckanext.welive.utils.generate_mapping')
    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    @mock.patch('ckanext.welive_utils.plugin.create_mkt')
    @mock.patch('requests.post')
    def test_update_mkt_mapping_true(self, mock_requests_post,
                                     mock_create_mkt, mock_send_dataset_log,
                                     mock_generate_mapping,
                                     mock_get_user_name_for_id,
                                     mock_get_email_for_ccuserid,
                                     mock_parse):

        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'
        mock_parse.return_value = self.MockedParse()

        mock_generate_mapping.return_value = json.dumps(MAPPING)

        dataset = create_dataset(organization_name='bilbao')
        create_resource(dataset=dataset)

        context = {}

        params = {'id': dataset['id']}
        dataset = helpers.call_action('package_show', context, **params)
        dataset['title'] = 'Updated title'

        helpers.call_action('package_update', context, **dataset)

        mkp_json = {'datasetid': dataset['name'], 'hasmapping': True,
                    'model': {'title': 'Updated title',
                              'description': 'No description provided',
                              'pilot': 'Bilbao',
                              'created': '2017/5/17',
                              'page': 'http://test-api//ods/dataset/test-package',
                              'tags': [],
                              '_abstract': 'No abstract provided',
                              'language': 'Spanish',
                              'hasBusinessRole': [{'title': 'Test Organization',
                                                   'page': 'http://test.ckan.net/organization/bilbao',
                                                   'businessRole': 'AUTHOR',
                                                   'mbox': 'foo@bar.com'}],
                              'hasLegalCondition': [{'title': None,
                                                     'description': None,
                                                     'url': None}]
                              },
                    'isPrivate': False}

        mock_requests_post.assert_called_with(
            '{}/mkp/update-dataset'.format(WELIVE_API),
            data=mock.ANY,
            auth=(BASIC_USER, BASIC_PASSWORD),
            headers={'Content-type': 'application/json'}
        )

        call = mock_requests_post.call_args
        self.assertDictEqual(mkp_json, json.loads(call[1]['data']))

    @mock.patch('dateutil.parser.parse')
    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckanext.welive.utils.generate_mapping')
    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    @mock.patch('ckanext.welive_utils.plugin.create_mkt')
    @mock.patch('requests.post')
    def test_update_mkt_mapping_false(self, mock_requests_post,
                                      mock_create_mkt, mock_send_dataset_log,
                                      mock_generate_mapping,
                                      mock_get_user_name_for_id,
                                      mock_get_email_for_ccuserid, mock_parse):

        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'
        mock_parse.return_value = self.MockedParse()
        mock_generate_mapping.return_value = json.dumps(MAPPING)

        dataset = create_dataset(organization_name='bilbao')
        create_resource(dataset=dataset)

        context = {}

        params = {'id': dataset['id']}
        dataset = helpers.call_action('package_show', context, **params)
        dataset['title'] = 'Updated title'
        dataset['resources'][0]['mapping'] = '{}'

        helpers.call_action('package_update', context, **dataset)

        mkp_json = {'datasetid': dataset['name'], 'hasmapping': False,
                    'model': {'title': 'Updated title',
                              'description': 'No description provided',
                              'pilot': 'Bilbao',
                              'created': '2017/5/17',
                              'page': 'http://test-api//ods/dataset/test-package',
                              'tags': [],
                              '_abstract': 'No abstract provided',
                              'language': 'Spanish',
                              'hasBusinessRole': [{'title': 'Test Organization',
                                                   'page': 'http://test.ckan.net/organization/bilbao',
                                                   'businessRole': 'AUTHOR',
                                                   'mbox': 'foo@bar.com'}],
                              'hasLegalCondition': [{'title': None,
                                                     'description': None,
                                                     'url': None}]
                              },
                    'isPrivate': False}

        mock_requests_post.assert_called_with(
            '{}/mkp/update-dataset'.format(WELIVE_API),
            data=mock.ANY,
            auth=(BASIC_USER, BASIC_PASSWORD),
            headers={'Content-type': 'application/json'}
        )

        call = mock_requests_post.call_args
        self.assertDictEqual(mkp_json, json.loads(call[1]['data']))


class TestDeleteDataset(unittest.TestCase):
    def setUp(self):
        model.repo.init_db()
        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    def test_delete_dataset(self, mock_send_dataset_log,
                            mock_get_user_name_for_id,
                            mock_get_email_for_ccuserid):
        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'

        dataset = create_dataset()
        context = {}
        params = {'id': dataset['id']}
        dataset = helpers.call_action('package_show', context, **params)

        params = {'id': dataset['id']}
        helpers.call_action('package_delete', context, **params)

        assert_raises(logic.NotFound, helpers.call_action,
                      'package_show', context, **params)

        calls = []
        calls.append(mock.call(mock.ANY, mock.ANY, 'Dataset created',
                               'DatasetPublished'))
        calls.append(mock.call(mock.ANY, mock.ANY, 'Dataset metadata accessed',
                               'DatasetMetadataAccessed'))
        calls.append(mock.call(mock.ANY, mock.ANY, 'Dataset removed',
                               'DatasetRemoved'))
        mock_send_dataset_log.assert_has_calls(calls, any_order=True)


class TestCreateResource(unittest.TestCase):

    def setUp(self):
        model.repo.init_db()
        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckanext.welive_utils.plugin.update_mkt')
    @mock.patch('ckanext.welive_utils.plugin.create_mkt')
    @mock.patch('ckanext.welive.utils.generate_mapping')
    @mock.patch('ckanext.welive_utils.plugin.send_resource_log')
    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    def test_create_resource(self, mock_send_dataset_log,
                             mock_send_resource_log, mock_generate_mapping,
                             mock_create_mkt, mock_update_mkt,
                             mock_get_user_name_for_id,
                             mock_get_email_for_ccuserid):

        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'
        mock_generate_mapping.return_value = json.dumps(MAPPING)

        resource = create_resource()

        self.assertEqual(resource['name'], RESOURCE_DATA['name'])
        self.assertEqual(resource['format'], RESOURCE_DATA['format'])
        self.assertDictEqual(json.loads(resource['mapping']), MAPPING)

        send_resource_log_calls = []
        send_resource_log_calls.append(mock.call(mock.ANY, mock.ANY,
                                                 'Resource created',
                                                 'ResourcePublished'))
        mock_send_resource_log.assert_has_calls(send_resource_log_calls,
                                                any_order=True)

        create_mkt_calls = []
        create_mkt_calls.append(mock.call(
            mock.ANY, {'package_id': resource['package_id'],
                       'name': RESOURCE_DATA['name'],
                       'format': RESOURCE_DATA['format'],
                       'url': 'http://foo/bar'})
            )
        mock_create_mkt.assert_has_calls(create_mkt_calls)


class TestMktNotification(unittest.TestCase):

    def setUp(self):
        model.repo.init_db()
        self.app = ckan.config.middleware.make_app(
            pylons_config['global_conf'], **pylons_config)
        self.app = webtest.TestApp(self.app)
        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    def _get_app(self):
        return self.app

    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('ckan.logic.action.get.user_show')
    @mock.patch('redis.StrictRedis.set')
    @mock.patch('redis.StrictRedis.exists')
    @mock.patch('requests.get')
    @mock.patch('requests.post')
    def test_mkt_notification_redis_exists_anon_user(self, mock_post, mock_get,
                                                     mock_redis_exists,
                                                     mock_redis_set,
                                                     mock_user_show,
                                                     mock_package_show):

        mock_package_show.return_value = {'type': 'dataset',
                                          'organization': {'title': 'Bilbao',
                                                           'name': 'bilbao'},
                                          'name': PACKAGE_DATA['name'],
                                          'title': PACKAGE_DATA['title'],
                                          'notes': '',
                                          'language': 'es',
                                          'id': 'packageID',
                                          'creator_user_id': 5}

        mock_user_show.return_value = {'name': '5'}
        mock_redis_exists.return_value = False
        mock_get.return_value = MockResponse(MKT_GET_RESPONSE)
        mock_post.return_value = MockResponse(MKT_POST_RESPONSE)

        with mock.patch.dict('ckanext.welive_utils.plugin.PILOT_DICT',
                             {'bilbao': 'Bilbao'}):

            plugin.create_mkt({}, {'package_id': 'packageID'})

            mock_get.assert_called_with('{}/mkp/get-all-artefacts/pilot-id/{}/'
                                        'artefact-types/Dataset'.format(
                                            WELIVE_API,
                                            'Bilbao'),
                                        auth=(BASIC_USER, BASIC_PASSWORD))

            mkp_json = {'title': PACKAGE_DATA['title'],
                        'description': 'No description provided',
                        'resourceRdf': '{}/ods/dataset/{}/usdl'.format(
                                WELIVE_API, PACKAGE_DATA['name']),
                        'webpage': '{}/ods/dataset/{}'.format(
                            WELIVE_API, PACKAGE_DATA['name']),
                        'providerName': 'Bilbao',
                        'datasetId': PACKAGE_DATA['name'],
                        'language': 'Spanish',
                        'ccUserId': 5,
                        'hasmapping': False}

            args = mock_post.call_args
            self.assertEqual('{}/mkp/create-dataset'.format(WELIVE_API),
                             args[0][0])
            self.assertEqual((BASIC_USER, BASIC_PASSWORD), args[1]['auth'])
            self.assertEqual({'Content-type': 'application/json'},
                             args[1]['headers'])
            self.assertDictEqual(mkp_json, json.loads(args[1]['data']))

            mock_redis_exists.assert_called_with(PACKAGE_DATA['name'])
            mock_redis_set.assert_called_with(PACKAGE_DATA['name'], 1567)

            mock_package_show.assert_called_with(mock.ANY, {'id': 'packageID'})
            mock_user_show.assert_called_with(mock.ANY, {'id': 5})

    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('ckan.logic.action.get.user_show')
    @mock.patch('redis.StrictRedis.set')
    @mock.patch('redis.StrictRedis.exists')
    @mock.patch('requests.get')
    @mock.patch('requests.post')
    def test_mkt_rest_notification(self, mock_post, mock_get,
                                   mock_redis_exists, mock_redis_set,
                                   mock_user_show,
                                   mock_package_show):

        mock_package_show.return_value = {'type': 'dataset',
                                          'organization': {'title': 'Bilbao',
                                                           'name': 'bilbao'},
                                          'name': PACKAGE_DATA['name'],
                                          'title': PACKAGE_DATA['title'],
                                          'notes': '',
                                          'language': 'es',
                                          'id': 'packageID',
                                          'creator_user_id': 5}

        mock_user_show.return_value = {'name': '5'}
        mock_redis_exists.return_value = False
        mock_get.return_value = MockResponse(MKT_GET_RESPONSE)
        mock_post.return_value = MockResponse(MKT_POST_RESPONSE)

        with mock.patch.dict('ckanext.welive_utils.plugin.PILOT_DICT',
                             {'bilbao': 'Bilbao'}):

            plugin.create_mkt({}, {'id': 'packageID'})

            mock_get.assert_called_with('{}/mkp/get-all-artefacts/pilot-id/{}/'
                                        'artefact-types/Dataset'.format(
                                            WELIVE_API,
                                            'Bilbao'),
                                        auth=(BASIC_USER, BASIC_PASSWORD))

            mkp_json = {'title': PACKAGE_DATA['title'],
                        'description': 'No description provided',
                        'resourceRdf': '{}/ods/dataset/{}/usdl'.format(
                                WELIVE_API, PACKAGE_DATA['name']),
                        'webpage': '{}/ods/dataset/{}'.format(
                            WELIVE_API, PACKAGE_DATA['name']),
                        'providerName': 'Bilbao',
                        'datasetId': PACKAGE_DATA['name'],
                        'language': 'Spanish',
                        'ccUserId': 5,
                        'hasmapping': False}

            args = mock_post.call_args
            self.assertEqual('{}/mkp/create-dataset'.format(WELIVE_API),
                             args[0][0])
            self.assertEqual((BASIC_USER, BASIC_PASSWORD), args[1]['auth'])
            self.assertEqual({'Content-type': 'application/json'},
                             args[1]['headers'])
            self.assertDictEqual(mkp_json, json.loads(args[1]['data']))

            mock_redis_exists.assert_called_with(PACKAGE_DATA['name'])
            mock_redis_set.assert_called_with(PACKAGE_DATA['name'], 1567)

            mock_package_show.assert_called_with(mock.ANY, {'id': 'packageID'})
            mock_user_show.assert_called_with(mock.ANY, {'id': 5})

    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('ckan.logic.action.get.user_show')
    @mock.patch('redis.StrictRedis.set')
    @mock.patch('redis.StrictRedis.exists')
    @mock.patch('requests.get')
    @mock.patch('requests.post')
    def test_mkt_existing_dataset(self, mock_post, mock_get,
                                  mock_redis_exists, mock_redis_set,
                                  mock_user_show,
                                  mock_package_show):

        mock_package_show.return_value = {'type': 'dataset',
                                          'organization': {'title': 'Bilbao',
                                                           'name': 'bilbao'},
                                          'name': 'existing-packageID',
                                          'title': PACKAGE_DATA['title'],
                                          'notes': '',
                                          'language': 'es',
                                          'id': 'packageID',
                                          'creator_user_id': 5}

        mock_user_show.return_value = {'name': '5'}
        mock_redis_exists.return_value = False
        mock_get.return_value = MockResponse(MKT_GET_RESPONSE)
        mock_post.return_value = MockResponse(MKT_POST_ERROR_RESPONSE)

        with mock.patch.dict('ckanext.welive_utils.plugin.PILOT_DICT',
                             {'bilbao': 'Bilbao'}):

            plugin.create_mkt({}, {'package_id': 'packageID'})

            mock_get.assert_called_with('{}/mkp/get-all-artefacts/pilot-id/{}/'
                                        'artefact-types/Dataset'.format(
                                            WELIVE_API,
                                            'Bilbao'),
                                        auth=(BASIC_USER, BASIC_PASSWORD))

            mock_post.assert_not_called()

            mock_redis_exists.assert_called_with('existing-packageID')
            mock_redis_set.assert_called_with('existing-packageID', 1567)

            mock_package_show.assert_called_with(mock.ANY, {'id': 'packageID'})
            mock_user_show.assert_not_called()

    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('ckan.logic.action.get.user_show')
    @mock.patch('redis.StrictRedis.set')
    @mock.patch('redis.StrictRedis.exists')
    @mock.patch('requests.get')
    @mock.patch('requests.post')
    def test_mkt_error(self, mock_post, mock_get, mock_redis_exists,
                       mock_redis_set, mock_user_show, mock_package_show):

        mock_package_show.return_value = {'type': 'dataset',
                                          'organization': {'title': 'Bilbao',
                                                           'name': 'bilbao'},
                                          'name': PACKAGE_DATA['name'],
                                          'title': PACKAGE_DATA['title'],
                                          'notes': '',
                                          'language': 'es',
                                          'id': 'packageID',
                                          'creator_user_id': 5,
                                          'hasmapping': False}

        mock_user_show.return_value = {'name': '5'}
        mock_redis_exists.return_value = False
        mock_get.return_value = MockResponse(MKT_GET_RESPONSE)
        mock_post.return_value = MockResponse(MKT_POST_ERROR_RESPONSE)

        with mock.patch.dict('ckanext.welive_utils.plugin.PILOT_DICT',
                             {'bilbao': 'Bilbao'}):

            plugin.create_mkt({}, {'package_id': 'packageID'})

            mock_get.assert_called_with('{}/mkp/get-all-artefacts/pilot-id/{}/'
                                        'artefact-types/Dataset'.format(
                                            WELIVE_API,
                                            'Bilbao'),
                                        auth=(BASIC_USER, BASIC_PASSWORD))

            mkp_json = {'title': PACKAGE_DATA['title'],
                        'description': 'No description provided',
                        'resourceRdf': '{}/ods/dataset/{}/usdl'.format(
                                WELIVE_API, PACKAGE_DATA['name']),
                        'webpage': '{}/ods/dataset/{}'.format(
                            WELIVE_API, PACKAGE_DATA['name']),
                        'providerName': 'Bilbao',
                        'datasetId': PACKAGE_DATA['name'],
                        'language': 'Spanish',
                        'ccUserId': 5,
                        'hasmapping': False}

            args = mock_post.call_args
            self.assertEqual('{}/mkp/create-dataset'.format(WELIVE_API),
                             args[0][0])
            self.assertEqual((BASIC_USER, BASIC_PASSWORD), args[1]['auth'])
            self.assertEqual({'Content-type': 'application/json'},
                             args[1]['headers'])
            self.assertDictEqual(mkp_json, json.loads(args[1]['data']))

            mock_redis_exists.assert_called_with(PACKAGE_DATA['name'])
            mock_redis_set.assert_not_called()

            mock_package_show.assert_called_with(mock.ANY, {'id': 'packageID'})
            mock_user_show.assert_called_with(mock.ANY, {'id': 5})

    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('ckan.logic.action.get.user_show')
    @mock.patch('redis.StrictRedis.set')
    @mock.patch('redis.StrictRedis.exists')
    @mock.patch('requests.get')
    @mock.patch('requests.post', side_effect=Exception())
    def test_mkt_exception(self, mock_post, mock_get, mock_redis_exists,
                           mock_redis_set, mock_user_show, mock_package_show):

        mock_package_show.return_value = {'type': 'dataset',
                                          'organization': {'title': 'Bilbao',
                                                           'name': 'bilbao'},
                                          'name': PACKAGE_DATA['name'],
                                          'title': PACKAGE_DATA['title'],
                                          'notes': '',
                                          'language': 'es',
                                          'id': 'packageID',
                                          'creator_user_id': 5,
                                          'hasmapping': False}

        mock_user_show.return_value = {'name': '5'}
        mock_redis_exists.return_value = False
        mock_get.return_value = MockResponse(MKT_GET_RESPONSE)
        mock_post.return_value = MockResponse(MKT_POST_RESPONSE)

        with mock.patch.dict('ckanext.welive_utils.plugin.PILOT_DICT',
                             {'bilbao': 'Bilbao'}):

            plugin.create_mkt({}, {'package_id': 'packageID'})

            mock_get.assert_called_with('{}/mkp/get-all-artefacts/pilot-id/{}/'
                                        'artefact-types/Dataset'.format(
                                            WELIVE_API,
                                            'Bilbao'),
                                        auth=(BASIC_USER, BASIC_PASSWORD))

            mkp_json = {'title': PACKAGE_DATA['title'],
                        'description': 'No description provided',
                        'resourceRdf': '{}/ods/dataset/{}/usdl'.format(
                                WELIVE_API, PACKAGE_DATA['name']),
                        'webpage': '{}/ods/dataset/{}'.format(
                            WELIVE_API, PACKAGE_DATA['name']),
                        'providerName': 'Bilbao',
                        'datasetId': PACKAGE_DATA['name'],
                        'language': 'Spanish',
                        'ccUserId': 5,
                        'hasmapping': False}

            args = mock_post.call_args
            self.assertEqual('{}/mkp/create-dataset'.format(WELIVE_API),
                             args[0][0])
            self.assertEqual((BASIC_USER, BASIC_PASSWORD), args[1]['auth'])
            self.assertEqual({'Content-type': 'application/json'},
                             args[1]['headers'])
            self.assertDictEqual(mkp_json, json.loads(args[1]['data']))

            mock_redis_exists.assert_called_with(PACKAGE_DATA['name'])
            mock_redis_set.assert_not_called()

            mock_package_show.assert_called_with(mock.ANY, {'id': 'packageID'})
            mock_user_show.assert_called_with(mock.ANY, {'id': 5})

            self.assertRaises(Exception, plugin.create_mkt)


class TestUpdateResource(unittest.TestCase):
    def setUp(self):
        model.repo.init_db()
        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckanext.welive_utils.plugin.update_mkt')
    @mock.patch('ckanext.welive_utils.plugin.create_mkt')
    @mock.patch('ckanext.welive.utils.generate_mapping')
    @mock.patch('ckanext.welive_utils.plugin.send_resource_log')
    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    def test_update_resource(self, mock_send_dataset_log,
                             mock_send_resource_log, mock_generate_mapping,
                             mock_create_mkt, mock_update_mkt,
                             mock_get_user_name_for_id,
                             mock_get_email_for_ccuserid):

        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'

        mock_generate_mapping.return_value = json.dumps(MAPPING)

        resource = create_resource()

        self.assertEqual(resource['name'], RESOURCE_DATA['name'])
        self.assertEqual(resource['format'], RESOURCE_DATA['format'])
        self.assertDictEqual(json.loads(resource['mapping']), MAPPING)

        context = {}
        params = {'id': resource['id'], 'name': 'Updated Test Resource',
                  'mapping': json.dumps(MAPPING_UPDATE)}
        helpers.call_action('resource_update', context, **params)

        params = {'id': resource['id']}
        resource = helpers.call_action('resource_show', context, **params)

        log.debug(resource)

        self.assertEqual(resource['name'], 'Updated Test Resource')
        self.assertEqual(resource['format'], RESOURCE_DATA['format'])
        self.assertEqual(json.loads(resource['mapping']), MAPPING_UPDATE)

        mock_send_resource_log.assert_any_call(
            mock.ANY, mock.ANY, 'Resource metadata updated',
            'ResourceMetadataUpdated')


class TestDeleteResource(unittest.TestCase):
    def setUp(self):
        model.repo.init_db()
        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    @mock.patch('ckanext.welive_utils.plugin.update_mkt')
    @mock.patch('ckanext.welive_utils.plugin.create_mkt')
    @mock.patch('ckanext.welive.utils.generate_mapping')
    @mock.patch('ckanext.welive_utils.plugin.send_resource_log')
    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    def test_delete_Resource(self, mock_send_dataset_log,
                             mock_send_resource_log, mock_generate_mapping,
                             mock_create_mkt, mock_update_mkt,
                             mock_get_user_name_for_id,
                             mock_get_email_for_ccuserid):

        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'

        resource = create_resource()
        context = {}
        params = {'id': resource['id']}
        helpers.call_action('resource_delete', context, **params)

        params = {'id': resource['id']}
        assert_raises(logic.NotFound, helpers.call_action, 'resource_show',
                      context, **params)

        mock_send_resource_log.assert_any_call(
            mock.ANY, mock.ANY, 'Resource removed', 'ResourceRemoved')


class TestSendLog(unittest.TestCase):
    def setUp(self):
        model.repo.init_db()
        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('requests.post')
    def test_send_dataset_log(self, mock_post):
        now = time.time()
        with mock.patch.object(time, 'time', return_value=now):
            mock_post.return_value = MockResponse('', status_code=200)

            pkg_dict = {'name': PACKAGE_DATA['name'],
                        'qa': {'openness_score': 3.0,
                               'openness_score_reason': 'reason'},
                        'id': 'packageID',
                        'title': PACKAGE_DATA['title'],
                        'ratings': 4.0,
                        'organization': {'name': 'bilbao'},
                        'type': 'dataset',
                        'private': False,
                        'format': 'CSV'
                        }

            user = MockedUser()

            plugin.send_log({'auth_user_obj': user}, pkg_dict, 'message',
                            'DatasetPublished', 'datasetid')

            log_data = {'msg': 'message', 'appId': APP_ID,
                        'type': 'DatasetPublished', 'timestamp': now,
                        'custom_attr': {'datasetid': PACKAGE_DATA['name'],
                                        'datasetopenness': 3.0,
                                        'datasetopennesreason': 'reason',
                                        'authorid': 5,
                                        'datasetname': PACKAGE_DATA['title'],
                                        'rating': 4.0,
                                        'pilot': 'Bilbao',
                                        'datasettype': 'Open Data',
                                        'resourcetype': 'CSV'}}

            args_post = mock_post.call_args

            self.assertEqual("%s/%s" % (LOGGING_URL, APP_ID), args_post[0][0])
            self.assertDictEqual(log_data, json.loads(args_post[1]['data']))
            self.assertDictEqual({'Content-type': 'application/json'},
                                 args_post[1]['headers'])

    @mock.patch('requests.post')
    def test_send_resource_log(self, mock_post):
        now = time.time()
        with mock.patch.object(time, 'time', return_value=now):
            mock_post.return_value = MockResponse('', status_code=200)

            pkg_dict = {'name': PACKAGE_DATA['name'],
                        'qa': {'openness_score': 3.0,
                               'openness_score_reason': 'reason'},
                        'id': 'packageID',
                        'title': PACKAGE_DATA['title'],
                        'ratings': 4.0,
                        'organization': {'name': 'bilbao'},
                        'type': 'dataset',
                        'private': False,
                        'format': 'CSV'
                        }

            user = MockedUser()

            plugin.send_log({'auth_user_obj': user}, pkg_dict, 'message',
                            'ResourcePublished', 'resourceid')

            log_data = {'msg': 'message', 'appId': APP_ID,
                        'type': 'ResourcePublished', 'timestamp': now,
                        'custom_attr': {'resourceid': 'packageID',
                                        'authorid': 5,
                                        'datasetname': PACKAGE_DATA['title'],
                                        'rating': 4.0,
                                        'pilot': 'Bilbao',
                                        'datasettype': 'Open Data',
                                        'resourcetype': 'CSV'}}

            args_post = mock_post.call_args

            self.assertEqual("%s/%s" % (LOGGING_URL, APP_ID), args_post[0][0])
            self.assertDictEqual(log_data, json.loads(args_post[1]['data']))
            self.assertDictEqual({'Content-type': 'application/json'},
                                 args_post[1]['headers'])

    @mock.patch('requests.post')
    def test_send_private_dataset_log(self, mock_post):
        now = time.time()
        with mock.patch.object(time, 'time', return_value=now):
            mock_post.return_value = MockResponse('', status_code=200)

            pkg_dict = {'name': PACKAGE_DATA['name'],
                        'qa': {'openness_score': 3.0,
                               'openness_score_reason': 'reason'},
                        'id': 'packageID',
                        'title': PACKAGE_DATA['title'],
                        'ratings': 4.0,
                        'organization': {'name': 'bilbao'},
                        'type': 'dataset',
                        'private': True,
                        'format': 'CSV'
                        }

            user = MockedUser()

            plugin.send_log({'auth_user_obj': user}, pkg_dict, 'message',
                            'DatasetPublished', 'datasetid')

            log_data = {'msg': 'message', 'appId': APP_ID,
                        'type': 'DatasetPublished', 'timestamp': now,
                        'custom_attr': {'datasetid': PACKAGE_DATA['name'],
                                        'datasetopenness': 3.0,
                                        'datasetopennesreason': 'reason',
                                        'authorid': 5,
                                        'datasetname': PACKAGE_DATA['title'],
                                        'rating': 4.0,
                                        'pilot': 'Bilbao',
                                        'datasettype': 'Private Data',
                                        'resourcetype': 'CSV'}}

            args_post = mock_post.call_args

            self.assertEqual("%s/%s" % (LOGGING_URL, APP_ID), args_post[0][0])
            self.assertDictEqual(log_data, json.loads(args_post[1]['data']))
            self.assertDictEqual({'Content-type': 'application/json'},
                                 args_post[1]['headers'])

    @mock.patch('requests.post')
    def test_send_social_dataset_log(self, mock_post):
        now = time.time()
        with mock.patch.object(time, 'time', return_value=now):
            mock_post.return_value = MockResponse('', status_code=200)

            pkg_dict = {'name': PACKAGE_DATA['name'],
                        'qa': {'openness_score': 3.0,
                               'openness_score_reason': 'reason'},
                        'id': 'packageID',
                        'title': PACKAGE_DATA['title'],
                        'ratings': 4.0,
                        'organization': {'name': 'bilbao'},
                        'type': 'dataset',
                        'private': True,
                        'format': 'CSV',
                        'extras': [
                                {'key': 'source', 'value': 'Social Network'}
                            ]
                        }

            user = MockedUser()

            plugin.send_log({'auth_user_obj': user}, pkg_dict, 'message',
                            'DatasetPublished', 'datasetid')

            log_data = {'msg': 'message', 'appId': APP_ID,
                        'type': 'DatasetPublished', 'timestamp': now,
                        'custom_attr': {'datasetid': PACKAGE_DATA['name'],
                                        'datasetopenness': 3.0,
                                        'datasetopennesreason': 'reason',
                                        'authorid': 5,
                                        'datasetname': PACKAGE_DATA['title'],
                                        'rating': 4.0,
                                        'pilot': 'Bilbao',
                                        'datasettype': 'Social Network',
                                        'resourcetype': 'CSV'}}

            args_post = mock_post.call_args
            self.assertEqual("%s/%s" % (LOGGING_URL, APP_ID), args_post[0][0])
            self.assertDictEqual(log_data, json.loads(args_post[1]['data']))
            self.assertDictEqual({'Content-type': 'application/json'},
                                 args_post[1]['headers'])

    @mock.patch('requests.post', side_effect=Timeout)
    def test_send_dataset_response_error_log(self, mock_post):
        now = time.time()
        with mock.patch.object(time, 'time', return_value=now):
            mock_post.return_value = MockResponse('', status_code=400)

            pkg_dict = {'name': PACKAGE_DATA['name'],
                        'qa': {'openness_score': 3.0,
                               'openness_score_reason': 'reason'},
                        'id': 'packageID',
                        'title': PACKAGE_DATA['title'],
                        'ratings': 4.0,
                        'organization': {'name': 'bilbao'},
                        'type': 'dataset',
                        'private': False,
                        'format': 'CSV'
                        }

            user = MockedUser()

            plugin.send_log({'auth_user_obj': user}, pkg_dict, 'message',
                            'DatasetPublished', 'datasetid')

            log_data = {'msg': 'message', 'appId': APP_ID,
                        'type': 'DatasetPublished', 'timestamp': now,
                        'custom_attr': {'datasetid': PACKAGE_DATA['name'],
                                        'datasetopenness': 3.0,
                                        'datasetopennesreason': 'reason',
                                        'authorid': 5,
                                        'datasetname': PACKAGE_DATA['title'],
                                        'rating': 4.0,
                                        'pilot': 'Bilbao',
                                        'datasettype': 'Open Data',
                                        'resourcetype': 'CSV'}}

            args_post = mock_post.call_args

            self.assertEqual("%s/%s" % (LOGGING_URL, APP_ID), args_post[0][0])
            self.assertDictEqual(log_data, json.loads(args_post[1]['data']))
            self.assertDictEqual({'Content-type': 'application/json'},
                                 args_post[1]['headers'])

            self.assertRaises(Timeout)


class TestRating(unittest.TestCase):
    def setUp(self):
        model.repo.init_db()
        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('ckanext.welive_utils.plugin.send_dataset_log')
    @mock.patch('ckanext.welive_utils.plugin.package_show')
    @mock.patch('ckan.logic.action.create.rating_create')
    def test_rating_create(self, mock_rating_create, mock_package_show,
                           mock_send_dataset_log):
        data_dict = {'package': 'test-package', 'rating': 4.0}
        plugin.rating_create({}, data_dict)

        mock_rating_create.assert_called_with({}, data_dict)
        mock_package_show.assert_called_with({}, {'id': data_dict['package']})
        mock_send_dataset_log.assert_called_with({}, mock.ANY,
                                                 'Dataset updated',
                                                 'DatasetMetadataUpdated')

    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('ckan.model.package.Package.get')
    def test_rating_show(self, mock_package_get, mock_package_show):
        user = MockedUser()
        session = MockedSession()
        mock_package_show.return_value = {'id': 'fake-package'}
        mock_package_get.return_value = MockedPackage()

        context = {'auth_user_obj': user, 'session': session}
        pkg_dict = {'id': 'fake-package'}
        rating_dict = plugin.rating_show(context, pkg_dict)

        self.assertDictEqual({'avg_rating': 3.0, 'rating_count': 2,
                             'user_rating': {'ccUserId': 5, 'rating': 4.0}},
                             rating_dict)

        mock_package_show.assert_called_with(context, pkg_dict)
        mock_package_get.assert_called_with(pkg_dict['id'])

    @mock.patch('ckan.logic.action.get.package_show',
                side_effect=logic.NotFound)
    def test_rating_show_package_missing(self, mock_package_show):
        with self.assertRaises(logic.NotFound):
            user = MockedUser()
            session = MockedSession()

            context = {'auth_user_obj': user, 'session': session}
            pkg_dict = {'id': 'fake-package'}
            plugin.rating_show(context, pkg_dict)

            mock_package_show.assert_called_with(context, pkg_dict)

    def test_rating_show_user_not_authorized(self):
        with self.assertRaises(logic.NotAuthorized):
            session = MockedSession()

            context = {'session': session}
            pkg_dict = {'id': 'fake-package'}
            plugin.rating_show(context, pkg_dict)

    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('ckan.model.package.Package.get')
    def test_rating_show_rating_none(self, mock_package_get,
                                     mock_package_show):
        user = MockedUser()
        session = MockedSession(rating=None)
        mock_package_show.return_value = {'id': 'fake-package'}
        mock_package_get.return_value = MockedPackage()

        context = {'auth_user_obj': user, 'session': session}
        pkg_dict = {'id': 'fake-package'}
        rating_dict = plugin.rating_show(context, pkg_dict)

        self.assertDictEqual({'avg_rating': 3.0, 'rating_count': 2,
                             'user_rating': {'ccUserId': 5, 'rating': None}},
                             rating_dict)

        mock_package_show.assert_called_with(context, pkg_dict)
        mock_package_get.assert_called_with(pkg_dict['id'])


class TestUserByEmail(unittest.TestCase):
    def setUp(self):
        model.repo.init_db()
        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('ckan.lib.dictization.model_dictize.user_dictize',
                side_effect=mock_user_dictize)
    def test_user_by_email(self, mock_user_dictize):
        user = factories.User(email='fake@mail.com')
        result_user = plugin.user_by_email({}, {'email': 'fake@mail.com'})

        self.assertEqual(user, result_user)
        mock_user_dictize.assert_called_with(user, {})

    @mock.patch('ckan.lib.dictization.model_dictize.user_dictize',
                side_effect=mock_user_dictize)
    def test_user_by_email_not_found(self, mock_user_dictize):
        with self.assertRaises(logic.NotFound):
            plugin.user_by_email({}, {'email': 'fake@mail.com'})


class TestUserDelete(unittest.TestCase):
    def post_return(self, url, auth):
        if url == 'http://test-api//lum/get-user-roles/5':
            return MockResponse(LUM_USER_ROLES)
        elif url == 'http://test-api//lum/get-user-company-details/ccuserid/5':
            return MockResponse(COMPANY_DETAILS)
        elif url == 'http://test-api//lum/get-leader-ccuserid-by-organizationid/organization-id/156':
            return MockResponse(ORGANIZATION_LEADER)

    def setUp(self):
        model.repo.init_db()

        app = ckan.config.middleware.make_app(
            pylons_config['global_conf'], **pylons_config)
        self.app = webtest.TestApp(app)

        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('requests.delete')
    @mock.patch('requests.post')
    def test_user_delete(self, mock_requests_post, mock_requests_delete):
        mock_requests_post.side_effect = self.post_return

        user = factories.User(email='fake@mail.com', name='05')

        context = {'auth_user_obj': user, 'model': model}
        params = {'id': user['id']}
        result_user = helpers.call_action('user_show', context, **params)

        apikey = user['apikey']
        del user['apikey']
        del user['email']

        self.assertDictEqual(user, result_user)

        tests.call_action_api(self.app, 'user_delete', apikey=apikey)

        mock_requests_delete.assert_called_with(
            '{}/ods/dataset/user/{}'.format(WELIVE_API, int(user['name'])),
            auth=(BASIC_USER, BASIC_PASSWORD))

        with self.assertRaises(logic.NotFound):
            user = helpers.call_action('user_show', context, **params)

    @mock.patch('requests.delete')
    @mock.patch('ckanext.welive_utils.plugin.user_by_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('requests.post')
    def test_user_delete_cascade(self, mock_requests_post,
                                 mock_get_email_for_ccuserid,
                                 mock_user_by_ccuserid, mock_requests_delete):
        mock_requests_post.side_effect = self.post_return
        mock_get_email_for_ccuserid.return_value = 'fake@mail.com'

        user = factories.User(email='fake@mail.com', name='05')
        leader = factories.User(id='random-id', email='leader@mail.com',
                                name='03')

        mock_user_by_ccuserid.return_value = leader

        organization = factories.Organization(
            extras=[{'key': 'orgID', 'value': '156'}])
        dataset = factories.Dataset(language='es',
                                    owner_org=organization['id'], user=user)

        # tests.call_action_api(self.app, 'user_delete', apikey=user['apikey'],
        #                       cascade='true')
        context = {'model': model, 'user': user['name']}
        helpers.call_action('user_delete', context=context)
        params = {'id': dataset['id']}
        dataset = helpers.call_action('package_show', context, **params)
        self.assertEqual(leader['id'], dataset['creator_user_id'])

        mock_requests_delete.assert_called_with(
            '{}/ods/dataset/user/{}'.format(WELIVE_API, int(user['name'])),
            auth=(BASIC_USER, BASIC_PASSWORD))

    def test_user_delete_not_found(self):
        context = {'model': model}
        params = {}
        with self.assertRaises(logic.NotAuthorized):
            helpers.call_action('user_delete', context, **params)


class TestPackageShow(unittest.TestCase):
    def post_return(self, url, auth):
        if url == 'http://test-api//lum/get-user-roles/5':
            return MockResponse(AUTHORITY_ROLE)
        elif url == 'http://test-api//lum/get-user-company-details/ccuserid/5':
            return MockResponse(COMPANY_DETAILS)
        elif url == 'http://test-api//lum/get-leader-ccuserid-by-organizationid/organization-id/156':
            return MockResponse(ORGANIZATION_LEADER)
        elif url == 'http://test-api//lum/get-user-roles/3':
            return MockResponse(DEVELOPER_ROLE)
        elif url == 'http://test-api//lum/get-user-company-details/ccuserid/3':
            return MockResponse(COMPANY_DETAILS)

    def setUp(self):
        model.repo.init_db()

        app = ckan.config.middleware.make_app(
            pylons_config['global_conf'], **pylons_config)
        self.app = webtest.TestApp(app)

        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    def test_package_show_public(self, mock_get_user_name_for_id,
                                 mock_get_email_for_ccuserid):
        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'
        dataset = create_dataset(
            {'name': 'test-package',
             'title': 'Test Package',
             'language': 'es',
             'private': False})
        user = factories.User(email='fake@mail.com', name='05')
        context = {'user': user['name'], 'model': model}
        result = helpers.call_auth(
            'package_show', context=context, id=dataset['id'])

        self.assertTrue(result)

    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    def test_package_show_private_anon_denied(self, mock_get_user_name_for_id,
                                              mock_get_email_for_ccuserid):

        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'
        dataset = create_dataset(
            {'name': 'test-package',
             'title': 'Test Package',
             'language': 'es',
             'private': True})

        with self.assertRaises(logic.NotAuthorized):
            context = {'user': None, 'model': model}
            helpers.call_auth(
                'package_show', context=context, id=dataset['id'])

    @mock.patch('requests.post')
    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    def test_package_show_private_authority(self, mock_get_user_name_for_id,
                                            mock_get_email_for_ccuserid,
                                            mock_requests_post):

        mock_get_user_name_for_id.return_value = '5'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'
        mock_requests_post.side_effect = self.post_return

        dataset = create_dataset(
            {'name': 'test-package',
             'title': 'Test Package',
             'language': 'es',
             'private': True}, organization_name='bilbao')

        user = factories.User(email='fake@mail.com', name='05')
        context = {'user': user['name'], 'model': model}
        result = helpers.call_auth(
            'package_show', context=context, id=dataset['id'])

        self.assertTrue(result)

    @mock.patch('requests.post')
    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    def test_package_show_private_developer(self, mock_get_user_name_for_id,
                                            mock_get_email_for_ccuserid,
                                            mock_requests_post):

        mock_get_user_name_for_id.return_value = '3'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'
        mock_requests_post.side_effect = self.post_return

        user = factories.User(email='fake@mail.com', name='03')
        context = {'user': user['name'], 'model': model}

        dataset = create_dataset(
            {'name': 'test-package',
             'title': 'Test Package',
             'language': 'es',
             'private': True}, organization_name='bilbao', context=context)

        result = helpers.call_auth(
            'package_show', context=context, id=dataset['id'])

        self.assertTrue(result)

    @mock.patch('requests.post')
    @mock.patch('ckanext.welive_utils.plugin.get_email_from_ccuserid')
    @mock.patch('ckanext.welive_utils.plugin.get_user_name_for_id')
    def test_package_show_private_company(self, mock_get_user_name_for_id,
                                          mock_get_email_for_ccuserid,
                                          mock_requests_post):

        mock_get_user_name_for_id.return_value = '3'
        mock_get_email_for_ccuserid.return_value = 'foo@bar.com'
        mock_requests_post.side_effect = self.post_return

        user = factories.User(email='fake@mail.com', name='03')
        context = {'user': user['name'], 'model': model}

        dataset = create_dataset(
            {'name': 'test-package',
             'title': 'Test Package',
             'language': 'es',
             'private': True}, organization_name='my-organization',
            organization_extras=[{'key': 'orgID', 'value': 156}])

        result = helpers.call_auth(
            'package_show', context=context, id=dataset['id'])

        self.assertTrue(result)


class TestOrganizationListForUser(unittest.TestCase):
    class MockedPylons():
        session = {'ckanext-welive-token': 'Bearer FAKE-TOKEN'}

    def post_return(self, url, auth):
        if url == 'http://test-api//lum/get-user-roles/5':
            return MockResponse(AUTHORITY_ROLE)
        elif url == 'http://test-api//lum/get-user-company-details/ccuserid/5':
            return MockResponse(json.dumps({}))
        if url == 'http://test-api//lum/get-user-roles/3':
            return MockResponse(LUM_USER_ROLES)
        elif url == 'http://test-api//lum/get-user-company-details/ccuserid/3':
            return MockResponse(json.dumps({}))
        if url == 'http://test-api//lum/get-user-roles/2':
            return MockResponse(CITIZEN_ROLE)
        elif url == 'http://test-api//lum/get-user-company-details/ccuserid/2':
            return MockResponse(COMPANY_DETAILS)
        elif url == 'http://test-api//lum/get-user-data/cc-user-id/5':
            return MockResponse(LUM_USER_DATA)

    def setUp(self):
        model.repo.init_db()

        app = ckan.config.middleware.make_app(
            pylons_config['global_conf'], **pylons_config)
        self.app = webtest.TestApp(app)

        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    def test_organization_list_for_user_anon(self):
        context = {'user': ''}
        result = helpers.call_action('organization_list_for_user', context)
        self.assertItemsEqual([], result)

    @mock.patch('requests.post')
    def test_organization_list_for_user_authority(self, mock_requests_post):
        mock_requests_post.side_effect = self.post_return

        user = factories.User(email='fake@mail.com', name='05')
        factories.Organization(name='bilbao')
        factories.Organization(name='novi-sad')
        factories.Organization(name='helsinki-uusimaa')
        factories.Organization(name='trento')

        context = {'user': user['name'], 'model': model}

        result = helpers.call_action('organization_list_for_user', context)

        name_list = []
        for organization in result:
            name_list.append(organization['name'])

        self.assertItemsEqual(
            ['bilbao'], name_list)

    @mock.patch('pylons.session')
    @mock.patch('requests.get')
    @mock.patch('requests.post')
    def test_organization_list_for_user_developer(self, mock_requests_post,
                                                  mock_requests_get,
                                                  mock_pylons):

        mock_requests_post.side_effect = self.post_return
        mock_requests_get.return_value = MockResponse(
            '{"referredPilot": "Bilbao"}')
        mock_pylons.return_value = {'ckanext-welive-token':
                                    'Bearer FAKE-TOKEN'}

        user = factories.User(email='fake@mail.com', name='03')
        factories.Organization(name='bilbao')
        factories.Organization(name='novi-sad')
        factories.Organization(name='helsinki-uusimaa')
        factories.Organization(name='trento')

        context = {'user': user['name'], 'model': model}

        result = helpers.call_action('organization_list_for_user', context)

        self.assertEqual('bilbao', result[0]['name'])

    @mock.patch('requests.post')
    def test_organization_list_for_user_company(self, mock_requests_post):
        mock_requests_post.side_effect = self.post_return

        user = factories.User(email='fake@mail.com', name='02')
        factories.Organization(name='my-company',
                               extras=[{'key': 'orgID', 'value': 156}])

        context = {'user': user['name'], 'model': model}

        result = helpers.call_action('organization_list_for_user', context)

        self.assertEqual('my-company', result[0]['name'])


class TestUtilsTestCase(unittest.TestCase):
    def setUp(self):
        model.repo.init_db()

        app = ckan.config.middleware.make_app(
            pylons_config['global_conf'], **pylons_config)
        self.app = webtest.TestApp(app)

        ckan.plugins.load('welive_utils')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive_utils')

    @mock.patch('ckanext.welive_authentication.plugin.get_user_email')
    @mock.patch('pylons.session')
    def test_get_user_email(self, mock_pylons, mock_get_user_email):
        mock_pylons.return_value = {'ckanext-welive-token':
                                    'Bearer FAKE-TOKEN'}

        mock_get_user_email.return_value = 'fake@mail.com'

        email = plugin.get_user_email()

        self.assertEqual('fake@mail.com', email)

    @mock.patch('requests.get')
    def test_get_email_from_ccuserid_welive(self, mock_get):
        mock_get.return_value = MockResponse(WELIVE_ACCOUNT)

        user = factories.User(email='fake@mail.com', name='05')
        email = plugin.get_email_from_ccuserid(user['id'])

        self.assertEqual('fake@mail.com', email)
        mock_get.assert_called_with(
            '{}/aac/accountprofile/profiles?userIds={}'.format(WELIVE_API,
                                                               user['id']),
            headers={'Authorization': 'Bearer {}'.format(CLIENT_TOKEN)})

    @mock.patch('requests.get')
    def test_get_email_from_ccuserid_gmail(self, mock_get):
        mock_get.return_value = MockResponse(GMAIL_ACCOUNT)

        user = factories.User(email='fake@mail.com', name='05')
        email = plugin.get_email_from_ccuserid(user['id'])

        self.assertEqual('fake@mail.com', email)
        mock_get.assert_called_with(
            '{}/aac/accountprofile/profiles?userIds={}'.format(WELIVE_API,
                                                               user['id']),
            headers={'Authorization': 'Bearer {}'.format(CLIENT_TOKEN)})

    @mock.patch('requests.get')
    def test_get_email_from_ccuserid_facebook(self, mock_get):
        mock_get.return_value = MockResponse(FACEBOOK_ACCOUNT)

        user = factories.User(email='fake@mail.com', name='05')
        email = plugin.get_email_from_ccuserid(user['id'])

        self.assertEqual('fake@mail.com', email)
        mock_get.assert_called_with(
            '{}/aac/accountprofile/profiles?userIds={}'.format(WELIVE_API,
                                                               user['id']),
            headers={'Authorization': 'Bearer {}'.format(CLIENT_TOKEN)})

    def test_get_organization_id(self):
        factories.Organization(name='my-company',
                               extras=[{'key': 'orgID', 'value': 156}])
        factories.Organization(name='another-company',
                               extras=[{'key': 'orgID', 'value': 165}])
        user = factories.User(email='fake@mail.com', name='05')
        context = {'model': model, 'user': user['name']}
        organization = plugin.get_organization_id(context, {'id': 156})

        self.assertEqual('my-company', organization['name'])
