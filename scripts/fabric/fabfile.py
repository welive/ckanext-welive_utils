from fabric.api import run
from fabric.contrib.files import exists

CKAN_LANG_DIR = '/usr/lib/ckan/default/src/ckan/ckan/i18n/'
EXTENSION_DIR = '/home/deusto/ckan/plugins'
LOCALE_DIR = '/home/deusto/locales/i18n'
LANG_LIST = ['en', 'es', 'it', 'sr_Latn', 'sr', 'fi']


def deploy_translations():
    output = run('ls {}'.format(EXTENSION_DIR))
    dir_list = output.split()
    for ext in dir_list:
        ext_dir = '{}/{}'.format(EXTENSION_DIR, ext)
        if exists('{}/i18n'.format(ext_dir)):
            for lang in LANG_LIST:
                if exists('{}/i18n/{}'.format(ext_dir, lang)):
                    run('msgcat --use-first "{0}/i18n/{1}/LC_MESSAGES/{2}.po" '
                        '"{3}/{1}/LC_MESSAGES/ckan.po" | msgfmt - -o '
                        '"{4}/{1}/LC_MESSAGES/ckan.mo"'.format(
                            ext_dir, lang, ext, CKAN_LANG_DIR, LOCALE_DIR))
